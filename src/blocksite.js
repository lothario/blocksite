﻿var BLOCKSITE = BLOCKSITE ||
    (function(jQuery){
	
        var defaultContainerOptions = {
            parent: 'body',
            cssId: 'BLOCKSITE',
            containerIsResizable: false,

            initialBlocks: 10,
            initialColor: 'random',

            style: {
                position: 'relative',
                width: '800px',
                height: '100%',
                border: '1px solid black',
                overflow: 'hidden'
            }

        };

        var defaultBlockOptions = {
            blockMinHeight: 50,
            blockMinWidth: 50,

            content: '',
            resizable: true,

            style: {
                backgroundColor: '',
                cursor: 'pointer',

                margin: '0px',

                borderColor: '#000000',
                borderWidth: '0px',
                borderRadius: '0px',
                borderStyle: 'solid',

                textAlign: 'center',
                float: 'left',
                display: 'inline-block',

                width: '100px',
                height: '100px',
                
                hover: {borderColor: '#888'}
            }
        };

		//Keep count of created blocks and containers
        var containerNo = 1;
        var blockNo = 1;
		
		var Container = function ( optionsObj ) {
            var self = this;

            this.options        = jQuery.extend(defaultContainerOptions, optionsObj);
            this.blockOptions   = jQuery.extend({}, defaultBlockOptions);
			this.blockClass 	= this.options.cssId + '-block';
            this.blockULId 	    = this.options.cssId + '-blockUL-' + containerNo;
            this.containerId    = this.options.cssId + '-container-' + containerNo;

            this.options.gridSize = 0.25 * (parseInt(this.blockOptions.style.width) + 2 * parseInt(this.blockOptions.style.margin) + 2 * parseInt(this.blockOptions.style.borderWidth));

            //Apply the blockUL style
            appendStyle(this.options.style, this.containerId, '');

            //Create the container
            $(this.options.parent).append('<div id="' + this.containerId + '" class="' + this.containerId + '"></div>');

            this.container = $('#' + this.containerId);

			//Create the block UL element
            this.container.append('<ul id="' + this.blockULId  + '" style="margin-left: auto; margin-right: auto; padding: 0; border-right: 20px solid white; border-left: 20px solid white; list-style-type: none;"></ul>');
			
			//Create the blockUL object for access
            this.blockUL = $('#' + this.blockULId);
            
			//Create the initial blocks
            this.addBlocksTo(this.blockUL, this.options.initialBlocks);
               
            appendStyle(this.blockOptions.style, this.blockClass, 'class');
            appendStyle(this.blockOptions.style.hover, this.blockClass + ':hover', 'class');

            this.createSettingsMenu();

            if(this.options.containerIsResizable) {
                $(this.blockUL).resizable();
            }

            this.update();

            this.blockUL.sortable({
                tolerance: "pointer",
                disabled: true
            });

            //Override jQuery UI's state disabled
            appendStyle({backgroundImage: 'none', opacity: 1}, 'ui-state-disabled', 'class');

            //Append edit icon
            this.container.before('<a id="' + this.options.cssId + '-editmode"href="#"><img style="vertical-align:middle" src="../images/doc_edit_icon&16.png" /></a>');

            $('#' + this.options.cssId + '-editmode').click(function(e){
                e.preventDefault();

                self.toggleEditMode();
            });

            $(document).keydown(function (e) {
                if (e.keyCode == 17) {
                    $('.' + self.blockClass).each(function(){
                       $(this).resizable('option', 'grid', [1,1]);
                    });
                }
            });

            $(document).keyup(function (e) {
                if (e.keyCode == 17) {
                    $('.' + self.blockClass).each(function(){
                        $(this).resizable('option', 'grid', [self.options.gridSize, self.options.gridSize]);
                    });
                }
            });

            containerNo++;
        };

        Container.prototype.add = function ( blockOptionsObj ) {
            this.blockUL.append('<li id="' + this.blockClass + '-' + blockNo + '" class="' + this.blockClass + '" style="background-color:' + blockOptionsObj.style.backgroundColor + ' ;"></li>');
            blockNo++;
        };
		
		Container.prototype.addBlocksTo = function ( parent, numBlocks ) {
			
            for ( var i = 0; i < numBlocks; i++ ) {

                if(this.options.initialColor === 'random') {
                    this.blockOptions.style.backgroundColor = '#' + Math.floor(Math.random() * 16777215).toString(16);
                } else {
                    this.blockOptions.style.backgroundColor = this.options.initialColor;
                }

                this.add(this.blockOptions);
            }
		};

        Container.prototype.toggleEditMode = function () {
            var allBlocks = $('.' + this.blockClass);

            $('#' +  this.options.cssId + '-settings').toggle();

            if(allBlocks.resizable('option', 'disabled')) {

                $('#' + this.options.cssId + '-editmode img').attr("src","../images/doc_edit_icon&16_end.png");

                allBlocks.resizable('option', 'disabled', false);

                this.blockUL.sortable('option', 'disabled', false);

                allBlocks.css('opacity', 0.5);
            } else {
                $('#' + this.options.cssId + '-editmode img').attr("src","../images/doc_edit_icon&16.png");
                allBlocks.resizable('option', 'disabled', true);

                this.blockUL.sortable('option', 'disabled', true);

                allBlocks.css('opacity', 1);
            }
        };

        Container.prototype.update = function () {
            $('.' + this.blockClass).resizable( {
                minHeight: this.blockOptions.blockMinHeight,
                minWidth: this.blockOptions.blockMinWidth,
                //If the container height is given in percent, don't limit the size of the block
                maxHeight: this.options.style.height.slice(-1) === '%' ? null: parseInt(this.options.style.height),
                maxWidth: this.options.style.width.slice(-1) === '%' ? null: parseInt(this.options.style.width) - 50,
                grid: [this.options.gridSize, this.options.gridSize],
                autoHide: true,
                disabled: true
            });

        };

        Container.prototype.export = function() {
            var exportedClone = this.container.clone();
            exportedClone.resizable('destroy');
            console.log(this.container.html());
        };
		
        Container.prototype.createSettingsMenu = function () {

            var cssId = this.options.cssId;

            var settings = $('<div id="' + cssId + '-settings"></div>');
            settings.append($('<a href="" id="' + cssId + '-settings-open"><img style="vertical-align:middle" src="../images/cog_icon&16.png" /><span style="margin-left: 6px;">Settings</span></a> <div id="'
                                    + cssId + '-settings-list">HELLO</div></div>'));

            this.container.append(settings);

            $('#' + cssId + '-settings').hide();

            var settingsStyle = {
                position: 'absolute',
                top: '0px',
                height: '100%',
                right: '0px'
            };

            var settingsListStyle = {
                width: '0px',
                height: '100%',
                float: 'right',
                overflow: 'hidden',
                border: '1px solid black',
                visibility: 'hidden',
                backgroundColor: '#fff',
                zIndex: '9999'
            };

            var settingsIconStyle = {
                margin: '5px 5px 5px 0',
                display: 'block',
                width: '16px',
                height: '26px',
                overflow: 'hidden',
                lineHeight: '26px',
                fontSize: '12px',
                textDecoration: 'none',
                textTransform: 'uppercase',
                whiteSpace: 'nowrap'
            };

            appendStyle(settingsStyle, cssId + '-settings', 'id');
            appendStyle(settingsListStyle, cssId + '-settings-list', 'id');
            appendStyle(settingsIconStyle, cssId + '-settings-open', 'id');

            $('#' + cssId + '-settings-open').click(function (event) {
                var duration = 600;
                event.preventDefault();
                if($('#' + cssId + '-settings-list').css('width') == '200px') {

                    $('#' + cssId + '-settings-list').animate({
                        width: '0px'
                    }, duration, 'swing', function() {
                        $(this).css('visibility', 'hidden');
                    });

                    $('#' + cssId + '-settings-open').animate({
                        width: '16px'
                    }, duration);


                } else {
                    $('#' + cssId + '-settings-list').css('visibility', 'visible');
                    $('#' + cssId + '-settings-list').animate({
                        width: '200px'
                    }, duration);

                    $('#' + cssId + '-settings-open').animate({
                        width: '221px'
                    }, duration);
                }

            });
        };

        function js2css(jsProp) {
            return jsProp.replace(/([A-Z])/g, "-$1").toLowerCase();
        }

        function appendStyle(styleObj, parent, type) {
            var rules = '';
            var selector = '#';
            var style = {};

            if(type === 'class') {
                selector = '.';
            }

            for ( var prop in styleObj ) {
                if(styleObj.hasOwnProperty(prop) ) {

                    var value = styleObj[prop];

                    if(typeof value != 'object') {
                        rules += js2css(prop) + ': ' + value + '; ';
                    }
                }
            }

            //Create a new style tag, if it doesn't exist, and append the css rules
            if ( $('style').length ){
                style = selector + parent + ' { ' + rules + ' } ';
                $('html > head > style').append(style);
            } else {
                style = $('<style> ' + selector + parent + ' { ' + rules + ' }</style>');
                $('html > head').append(style);
            }
        }
		

        return {
             Container: Container
         }
    })(jQuery);